import os
import sys

job_1_name = os.environ.get('JOB_1_NAME')
job_2_name = os.environ.get('JOB_2_NAME')

yaml_template = f'''
{job_1_name}:
  image: alpine:3.10
  when: manual
  script:
    - echo "hello from {job_1_name}, $message_1"

{job_2_name}:
  image: alpine:3.10
  when: manual
  script:
    - echo "hello from {job_2_name}, $message_2"
'''

f = open(sys.argv[1], "a")
f.write(yaml_template)
f.close()